from flet import *
from fleter import *
from flet_core import *

readme = """
# 2201班 工具箱
这里可以使用点名软件辅助老师工作，也可以在论坛里畅所欲言


"""


def rollcall(list: list):
    container_rollcall = Container()
    return container_rollcall


def create_readme_page(event):
    tabs.add_control_with_can_close(text="简介", content=Container(Markdown(value=readme, expand=True), padding=10))
    tabs.update()


def copy(data):
    page2.set_clipboard(data)
    page2.snack_bar = SnackBar(
        Text(f"复制成功！（复制内容：{data}）", color=colors.WHITE),
        bgcolor=colors.DEEP_PURPLE,
        action="确定",
        action_color=colors.WHITE
    )
    page2.snack_bar.open = True

    page2.update()


def home():
    readme_doc = Markdown(value=readme)
    container_home = Container(
        Column(
            [
                Card(
                    Container(
                        ListTile(
                            leading=Icon(icons.SHARE_LOCATION_ROUNDED),
                            title=Text("简介"),
                            subtitle=Text("简介就是简介，没什么好简介的"),
                            on_click=create_readme_page
                        )
                    ),
                ),
                Card(
                    Container(
                        Column(
                            [
                                ListTile(
                                    leading=Icon(icons.ADS_CLICK_ROUNDED),
                                    title=Text("点名"),
                                    subtitle=Text("更多模式请通过指令打开（跟多指令需要私信获取）"),
                                    on_click=create_rollcall_default
                                ),
                                Row(
                                    alignment=MainAxisAlignment.END,
                                )
                            ]
                        )
                    ),
                ),
                Row(
                    [
                        Card(
                            Row(
                                [
                                    CircleAvatar(
                                        content=Text("XQX"),
                                    ),
                                    Text("QQ：1379773753"),
                                    TextButton("复制", on_click=lambda event: copy("1379773753")),
                                    Text("Email：XiangQinxi@outlook.com"),
                                    TextButton("复制", on_click=lambda event: copy("XiangQinxi@outlook.com")),
                                    ]
                            ),
                        ),

                    ]
                ),
            ],
            scroll=True,
            auto_scroll=True,

        ),
        padding=10,
    )

    return container_home


def create_readme(event):
    tabs.add_control_with_can_close(text="简介", content=Container(Markdown(value=readme, expand=True), padding=10))
    tabs.update()


def create_rollcall_default(event):
    tabs.add_control_with_can_close(text="点名：默认", content=rollcall(["李姝芃", "张嘉琪", "胡语凌"]))
    tabs.update()


def create_dev1(event):
    tabs.update()


def main(page: Page):
    global tabs, page2

    page2 = page

    light_theme = theme.Theme(color_scheme_seed=colors.DEEP_PURPLE, use_material3=True)
    dark_theme = theme.Theme(color_scheme_seed=colors.DEEP_PURPLE, use_material3=True)

    page.theme = light_theme
    page.dark_theme = dark_theme

    command_entry = TextField(border_radius=20)

    def run(event):
        command_dialog.open = False

        if command_entry.value == "114514":
            print("指令读取成功！已创建“点名：张老师”板块")
            tabs.add_control_with_can_close("点名：张老师")
        elif command_entry.value == "dev1":
            from fleter_example.colors_browser.v2 import ColorBrowser2
            print("指令读取成功！已创建“开发：调试1”板块")
            tabs.add_control_with_can_close(text="开发：调试1", content=ColorBrowser2(page))
        page.update()

    command_dialog = AlertDialog(
        title=Row(
            [
                Text("输入你的指令")
            ]
        ),
        content=command_entry,
        actions=[
            FilledButton(
                "执行",
                on_click=run
            )
        ],
        actions_alignment=MainAxisAlignment.END
    )

    def command(event: FilledButton):
        page.dialog = command_dialog
        command_dialog.open = True
        page.window_focused = True
        page.update()

    page.window_title_bar_buttons_hidden = True
    page.window_title_bar_hidden = True

    page.auto_scroll = True

    closebutton = CloseButton(page)
    closebutton.icon_color = colors.WHITE

    maxbutton = MaximizeButton(page, icon=icons.FULLSCREEN_ROUNDED, icon_max=icons.FULLSCREEN_EXIT_ROUNDED)
    maxbutton.icon_color = colors.WHITE

    page.appbar = AppBar(
        bgcolor=colors.DEEP_PURPLE,
        title=WindowDragArea(
            Row(
                [
                    Icon(icons.LOCAL_FLORIST_OUTLINED, color=colors.WHITE),
                    Text("2201班", color=colors.WHITE),
                    Text("@2023 XiangQinxi", size=13, text_align=TextAlign.START, color=colors.WHITE)
                ]
            )
        ),
        actions=[
            FilledButton(
                text="指令",
                on_click=command,
            ),
            VerticalDivider(color=colors.DEEP_PURPLE_400),
            PopupMenuButton(
                content=Icon(icons.MORE_VERT_ROUNDED, color=colors.WHITE),
                items=[
                    SwitchThemePopupMenuItem(page, text="切换深浅主题", icon=icons.BRIGHTNESS_5)
                ],

            ),
            VerticalDivider(color=colors.DEEP_PURPLE_400),
            maxbutton,
            closebutton
        ],
    )

    tabs = NoteBook()

    tabs.add_control(text="主页", content=home())

    page.add(
        tabs,
    )

    page.theme_mode = "dark"

    page.update()


import logging
logging.basicConfig(level=logging.DEBUG)
# app(target=main, view=WEB_BROWSER, port=35600)
app(target=main)
